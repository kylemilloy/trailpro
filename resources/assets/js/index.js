import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { App } from './containers';
import { Home, Error } from './views';

const store = createStore( combineReducers({
  routing: routerReducer
}));
const history = syncHistoryWithStore( browserHistory, store );

render( 
  <Provider store={ store }>
    <Router history={ history }>
      <Route path="/" component={ App }>
        <IndexRoute component={ Home } />
        <Route state={ 404 } path="*" component={ Error } />
      </Route>
    </Router>
  </Provider>,
  document.getElementById( 'mount-point' ) 
);