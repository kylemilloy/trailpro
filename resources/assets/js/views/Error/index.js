import React from 'react';
export default class Error extends React.Component {
  
  constructor( props ) {
    super( props );
    this.errorCode = this.props.route.state;
  }
  
  error404() {
    return (
      <div>
        Error 404.
      </div>
    );
  }
  
  error() {
    return(
      <div>
        Generic error.
      </div>
    )
  }
  
  render() {
    switch( this.errorCode ) {
      case 404:
        return this.error404();
      default:
        return this.error();
    }
  }
}