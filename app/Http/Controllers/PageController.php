<?php

namespace App\Http\Controllers;

class PageController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    
  }
  
  /**
   * Returns base view for React App.
   *
   * @return view()
   */
  public function index() {
    return view( 'app' );
  }
}
